<h3 align="">Equipo:</h3>
<p align=""><strong><code>Brayn Alejandro Torres Villarreal</code></strong></p>

<h3 align="center">Mii App: Producto Integrador de Aprendizaje</h3>
<p align="center"><strong><code>APPMOV_PIA</code></strong></p>

## Installation

To start building your Mii App, clone this repo to a new directory:

```bash
git clone https://gitlab.com/BTorresRoyal/appmov_pia.git
cd APPMOV_PIA
```

 - then install it

```bash
npm install
```

 - then go to the building process

```bash
ionic build
npx cap sync
```

### Building Native Project


#### Android

```bash
npx cap open android
```
Once Android Studio launches, you can build your app through the standard Android Studio workflow.

