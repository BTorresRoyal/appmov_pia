import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@capacitor/splash-screen'
import { Subscription } from 'rxjs';
import { LoginService } from './login/login.service';
import { AmiiboService } from './services/amiibo.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  private loginSub: Subscription;
  private lastState = false;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private amiiboService: AmiiboService
  ) {
    //this.initializeApp();
  }

  initializeApp() {
    /* To make sure we provide the fastest app loading experience
       for our users, hide the splash screen automatically
       when the app is ready to be used:

        https://capacitor.ionicframework.com/docs/apis/splash-screen#hiding-the-splash-screen
    */
    SplashScreen.hide();
  }

  ngOnInit(){
    
    this.loginSub = this.loginService.usuarioLoggedo.subscribe( isAuth => {
      if(!isAuth && this.lastState !== isAuth){
        this.router.navigateByUrl('/login');
      }
      this.lastState = isAuth;
    });
    this.amiiboService.fetchAmiibo();
  }

  ngOnDestroy(){
    if(this.loginSub){
      this.loginSub.unsubscribe();
    }
  }

  onLogout() {
    this.loginService.logout();
    this.router.navigateByUrl('/login');
  }
}
