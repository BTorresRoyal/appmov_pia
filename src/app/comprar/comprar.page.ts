import { Component, OnInit } from '@angular/core';
import { AmiiboModel } from '../services/amiibo.model';
import { AmiiboService } from '../services/amiibo.service';

@Component({
  selector: 'app-comprar',
  templateUrl: './comprar.page.html',
  styleUrls: ['./comprar.page.scss'],
})
export class ComprarPage implements OnInit {

  constructor(
    private amiiboService: AmiiboService
  ) { }

  async ngOnInit() {
    await this.amiiboService.cargar();
  }

}
