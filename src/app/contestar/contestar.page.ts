import { Component} from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { AmiiboModel } from '../services/amiibo.model';
import { AmiiboService } from '../services/amiibo.service';
import { UserPhoto, PhotoService } from '../services/photo.service';

@Component({
  selector: 'app-contestar',
  templateUrl: './contestar.page.html',
  styleUrls: ['./contestar.page.scss'],
})
export class ContestarPage{
  amiibo: AmiiboModel[] = [];

  constructor(private amiiboService:AmiiboService, public photoService: PhotoService, public actionSheetController: ActionSheetController) { }

  async ngOnInit() {
    this.amiibo = this.amiiboService.amiibo;
    await this.photoService.loadSaved();
  }

  public async showActionSheet(photo: UserPhoto, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deletePicture(photo, position);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
         }
      }]
    });
    await actionSheet.present();
  }
}
