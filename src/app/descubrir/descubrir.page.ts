import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AmiiboModel, AmiiboProps } from '../services/amiibo.model';
import { AmiiboService } from '../services/amiibo.service';

@Component({
  selector: 'app-descubrir',
  templateUrl: './descubrir.page.html',
  styleUrls: ['./descubrir.page.scss'],
})
export class DescubrirPage {
  amiiboProps: AmiiboProps;
  amiibo: AmiiboModel[];
  amiiboBuscado: AmiiboModel[] = [];
  serie: string = "";

  @ViewChild('myForm') myForm: NgForm;

  constructor(private amiiboService: AmiiboService) {}

  ngOnInit() {
    this.amiiboProps = this.amiiboService.amiiboProps;
    this.amiibo = this.amiiboService.amiibo;
  }

  customPopoverOptions: any = {
    header: 'AmiiboSeries:',
  };

  buscar(){
    console.log("buscando");
    let nombre = this.myForm.value['nombre'];
    let serie = this.serie;
    this.amiiboBuscado = [];
    let arr = [];
    if(nombre && serie) {
      this.amiibo.forEach( el => {
        if(el.amiiboSeries == serie){
          arr.push(el);
          console.log(el.amiiboSeries);
        }  
      })
      arr.forEach( el => {
        let condition = el.name.toLowerCase().includes(nombre.toLowerCase());
        if (condition)
          this.amiiboBuscado.push(el);//Corregir
      })
    }
    if(nombre && !serie){
      this.amiibo.forEach(el => {
        let condition = el.name.toLowerCase().includes(nombre.toLowerCase());
        if (condition)
          this.amiiboBuscado.push(el);
      });
    }
    if(!nombre && serie){
      this.amiibo.forEach( el => {
        if(el.amiiboSeries == serie){
          this.amiiboBuscado.push(el);
          console.log(el.amiiboSeries);
        }  
      })
    }
    console.log(this.amiiboBuscado, nombre);
  }

  select(option: any){
    this.serie = option.target.value;
    console.log(this.serie);
    
  }

}
