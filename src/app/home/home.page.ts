import { Component, OnInit } from '@angular/core';
import { AmiiboModel } from '../services/amiibo.model';
import { AmiiboService } from '../services/amiibo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  amiibo: AmiiboModel[] = [];
  month = new Date().getMonth();

  constructor(
    private amiiboService: AmiiboService
  ) { }

  ngOnInit() {
    this.amiibo = this.amiiboService.amiibo;
  }

}
