export interface AmiiboModel {
    id: number;
    amiiboSeries: string;
    character: string;
    gameSeries: string;
    image: string;
    name: string;
}

export interface AmiiboProps {
    amiiboSeries: string[];
    character: string[];
    gameSeries: string[];
    image: string[];
    name: string[];
}