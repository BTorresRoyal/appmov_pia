import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';
import { AmiiboModel, AmiiboProps } from './amiibo.model';

@Injectable({
  providedIn: 'root',
})
export class AmiiboService {
  private COMPRA_STORAGE: string = 'compras';
  isLoading: boolean = false;
  amiibo: AmiiboModel[] = [];
  amiiboProps: AmiiboProps;
  amiiboCompra: AmiiboModel[] = [];

  constructor() {}

  async fetchAmiibo() {
    this.isLoading = true;
    this.amiiboProps = {
      amiiboSeries: [],
      character: [],
      gameSeries: [],
      image: [],
      name: []
    };
    let url = 'https://amiiboapi.com/api/amiibo/';
    let res = await fetch(url);
    let data = await res.json();
    let arr = await data.amiibo;
    let id = 0;
    arr.forEach((element) => {
      let { amiiboSeries, character, gameSeries, image, name } = element;
      this.amiibo[id] = {
        id,
        amiiboSeries,
        character,
        gameSeries,
        image,
        name,
      };
      id++;
      for (let p in this.amiiboProps) this.amiiboProps[p].push(element[p]);
    });
    for (let p in this.amiiboProps)
      this.amiiboProps[p] = Array.from(new Set(this.amiiboProps[p]));
    console.log(this.amiibo, this.amiiboProps);
    this.isLoading = false;
  }

  agregar(mii: AmiiboModel){
    this.amiiboCompra.push(mii);
    Storage.set({
      key: this.COMPRA_STORAGE,
      value: JSON.stringify(this.amiiboCompra)
    });
  }
  remover(mii: AmiiboModel){
    let i = 0;
    this.amiiboCompra.forEach(el => {
      if(el.id == mii.id)
        this.amiiboCompra.splice(i, 1)
      i++;
    });
    Storage.set({
      key: this.COMPRA_STORAGE,
      value: JSON.stringify(this.amiiboCompra)
    });
  }
  async cargar(){
    let compras = await Storage.get({ key: this.COMPRA_STORAGE});
    this.amiiboCompra = JSON.parse(compras.value) || [];

  }
}
