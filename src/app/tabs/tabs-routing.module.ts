import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../home/home.module').then((m) => m.HomePageModule),
          }
        ]
      },
      {
        path: 'contestar',
        children: [
          {
            path: '',
            loadChildren: () => 
              import('../contestar/contestar.module').then((m) => m.ContestarPageModule)
          }
        ]
      },
      {
        path: 'descubrir',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../descubrir/descubrir.module').then((m) => m.DescubrirPageModule),
          }
        ]
      },
      {
        path: 'comprar',
        children: [
          {
            path:'',
            loadChildren: () =>
              import('../comprar/comprar.module').then((m) => m.ComprarPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full',
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
